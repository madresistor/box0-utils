/*
 * This file is part of box0-utils.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <argp.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <signal.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>

/* TODO: only operation = {scan} is implemented */
/* TODO: CTRL-c (ie SIGINT) should show a new prompt and discard previously given input */
/* TODO: option to show as table like i2c-detect (for scan) */

static bool execute_command_and_ret_status(b0_i2c *mod, char *cmd);
static void show_repl(b0_i2c *mod);

static char *the_generator(const char *text, int state);
static char **the_completion(const char *text , int start,  int end);

const char *argp_program_version =
	"box0-i2c 0.0.1";
const char *argp_program_bug_address =
	"<kuldeep@madresistor.com>";

/* Program documentation. */
static char doc[] =
	"Box0 I2C -- CLI interface Box0 I2C\v"
	"MODULE_INDEX default = 0\n"
	"COMMAND = Execute command";

/* A description of the arguments we accept. */
static char args_doc[] = "COMMAND";

/* The options we understand. */
static struct argp_option options[] = {
	{"module",	'm', "MODULE_INDEX",0,  "Module index" },
	{"command",	'e', "COMMAND",0,  "Command" },
	{ 0 }
};

/* Used by main to communicate with parse_opt. */
struct arguments {
	int module_index;
	size_t cmd_count;
	char **cmd;
};

/**
 * Append command @a cmd to command list in @a arg
 * @param arg Argument
 * @param cmd Command to append
 */
static void append_cmd(struct arguments *arg, char *cmd)
{
	arg->cmd_count++;
	arg->cmd = realloc(arg->cmd, sizeof(*arg->cmd) * arg->cmd_count);
	arg->cmd[arg->cmd_count - 1] = strdup(cmd);
}

/**
 * Merge string form @a arg till NULL is reached with [space] as joiner
 * @param arg Pointer to string array
 */
static char *merge_string_till_null(char **arg)
{
	char *prev_cmd = NULL;
	size_t prev_len = 0;

	while (*arg) {
		char *data = *arg;
		size_t data_len = strlen(*arg);

		if (!data_len) {
			continue;
		}

		if (prev_cmd == NULL) {
			prev_cmd = realloc(prev_cmd, data_len + 1);
			memcpy(prev_cmd, data, data_len);
			prev_len = data_len;
			prev_cmd[data_len] = '\0';
		} else {
			size_t len = data_len + prev_len + 1;
			prev_cmd = realloc(prev_cmd, len + 1);
			memcpy(&prev_cmd[prev_len + 1], data, data_len);
			prev_cmd[prev_len] = ' ';
			prev_cmd[len] = '\0';
			prev_len = len;
		}

		arg++;
	}

	return prev_cmd;
}

/* Parse a single option. */
static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
	 know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;

	switch(key) {
	case 'm':
		arguments->module_index = atoi(arg);
	break;

	case 'e':
		append_cmd(arguments, arg);
	break;

	case ARGP_KEY_ARG:
		if (!arguments->cmd_count) {
			arg = merge_string_till_null(&state->argv[state->next - 1]);
			append_cmd(arguments, arg);
			state->next = state->argc;
		} else {
			/* Illegal */
			argp_usage(state);
		}
	break;

	case ARGP_KEY_END:
	break;

	default:
	return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

int main(int argc, char **argv)
{
	b0_device *device;
	b0_i2c *module;

	struct arguments arguments;

	/* Default values. */
	arguments.module_index = 0;
	arguments.cmd_count = 0;
	arguments.cmd = NULL;

	/* Parse our arguments; every option seen by parse_opt will be reflected in arguments. */
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	/* try to minimize garbage on screen */
	setenv("LIBBOX0_LOG", "w", 0);

	/* open device */
	if (B0_ERROR_RESULT_CODE(b0_usb_open_supported(&device))) {
		fprintf(stderr, "unable to open device\n");
		goto done;
	}

	/* open module */
	if (B0_ERROR_RESULT_CODE(b0_i2c_open(device, &module, arguments.module_index))) {
		fprintf(stderr, "unable to open module\n");
		goto close_device;
	}

	/* prepare for master mode */
	if (B0_ERROR_RESULT_CODE(b0_i2c_master_prepare(module))) {
		fprintf(stderr, "unable to prepare module\n");
		goto close_module;
	}

	size_t i;
	for (i = 0; i < arguments.cmd_count; i++) {

		/* hardwired in genes to exit */
		if (!strcmp(arguments.cmd[i], "exit")) {
			goto close_module;
		}

		if (!execute_command_and_ret_status(module, arguments.cmd[i])) {
			fprintf(stderr, "Command `%s` failed\n", arguments.cmd[i]);
			break;
		}

		/* free the argument */
		free(arguments.cmd[i]);
	}

	if (arguments.cmd_count) {
		/* free the command array */
		free(arguments.cmd);
	}

	/* show REPL if not commands provided */
	if (!arguments.cmd_count) {
		show_repl(module);
	}

	close_module:
	if (B0_ERROR_RESULT_CODE(b0_i2c_close(module))) {
		fprintf(stderr, "unable to close module\n");
	}

	close_device:
	if (B0_ERROR_RESULT_CODE(b0_device_close(device))) {
		fprintf(stderr, "unable to close device\n");
	}

	done:
	exit (0);
}

struct help_list_value {
	const char *name;
	const char *desc;
};

static const struct help_list_value help_list[] = {
	{"help", "This Help"},
	{"?", "short-hand version of `help'"},
	{"exit", "Exit the program"},
	{"scan", "Scan all possible address"},
	{"scan addr...", "scan all given addresses"},
	{"scan addr/len...", "scan all given address range, addr/len = addr+0, addr+1, ... addr+len-1 And len > 0"},
	{"scan addr[/len]...", "mix of addresses and address range"},
	{"show <i>", "Show information  <i> can be 'c' or 'w'"},
	{"hw", "Show hardware information"},
	{"read <addr> <bytes-count>", "Read <bytes-count> data from slave at <addr>"},
	{"r <addr> <bytes-count>", "short-hand version of `read'"},
	{"write8read <addr> <write-byte> <bytes-count>",
			"First write <write-byte> to slave at <addr> and then read <bytes-count> data"},
	{"w8r <addr> <write-byte> <bytes-count>", "short hand version of `write8read'"},
	{"write <addr> <data>...", "Write <data>'s to slave at <addr>"},
	{"w <addr> <data>...", "short hand version of `write'"},
	{NULL}
};

/**
 * show help
 * @return true
 */
static bool command_help()
{
	const struct help_list_value *i = help_list;
	while (i->name != NULL) {
		printf("%s  %s\n", i->name, i->desc);
		i++;
	}

	return true;
}

static bool command_warranty(b0_i2c *mod, char *cmd)
{
	const char *msg =
	"THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n";

	puts(msg);
	return true;
}

static bool command_copyright(b0_i2c *mod, char *cmd)
{
	const char *msg = "TODO: place https://gnu.org/licenses/gpl.html\n";
	puts(msg);
	return true;
}

/**
 * @param mod Module
 * @param slave_addr Slave Address
 * @return true on found
 * @return false on not found
 */
static bool _command_scan_single(b0_i2c *mod, uint8_t slave_addr)
{
	bool slave_present;
	const b0_i2c_sugar_arg arg = {
		.addr = slave_addr,
		.version = B0_I2C_VERSION_SM /* FIXME: make this configurable */
	};

	if (B0_ERR_RC(b0_i2c_master_slave_detect(mod, &arg, &slave_present))) {
		fprintf(stderr, "unable to query slave status (0x%"PRIX8")\n", slave_addr);
		return false;
	} else if (slave_present) {
		printf("Slave At: 0x%"PRIX8"\n", slave_addr);
		/* TODO: print some more info on slave */
		return true;
	}
}

/**
 * Scan the bus for all possible slaves
 * @param mod Module
 * @param start Start Address
 * @param end End address (one before last)
 * @param stride Number of element to skip for next
 * @return number of slaves found
 */
static unsigned _command_scan(b0_i2c *mod,
	uint8_t start, uint8_t end, uint8_t stride)
{
	unsigned count = 0;
	uint8_t slave_addr;
	bool slave_present;

	for (slave_addr = start; slave_addr < end; slave_addr += stride) {
		if (_command_scan_single(mod, slave_addr)) {
			count += 1;
		}
	}

	return count;
}

/**
 * Scan for slave on bus with address in @a cmd
 * @param mod Module
 * @param cmd space seperated, slave address
 * @return number of slaves found
 */
static unsigned _command_scan_addr(b0_i2c *mod, char *cmd)
{
	unsigned count = 0;

	char *token;
	while ((token = strsep(&cmd, " ")) != NULL) {

		char *endptr;

		char *str_addr = strsep(&token, "/");
		errno = 0;
		int slave_addr = strtol(str_addr, &endptr, 0);
		/* TODO: 10bit address support */

		if (errno) {
			fprintf(stderr, "address: strtol failed\n");
			continue;
		} else if (endptr == token) {
			fprintf(stderr, "address: no digits found\n");
			continue;
		} else if (slave_addr < 0 || slave_addr >= 0x7F) {
			fprintf(stderr, "not a valid 7bit address: \"%s\"\n", str_addr);
			continue;
		}

		if (token == NULL) {
			if (_command_scan_single(mod, slave_addr)) {
				count += 1;
			}
		} else {
			errno = 0;
			int len = strtol(token, &endptr, 0);
			if (errno) {
				fprintf(stderr, "length: strtol failed\n");
				continue;
			} else if (endptr == token) {
				fprintf(stderr, "length: no digits found\n");
				continue;
			} else if (len <= 0) {
				fprintf(stderr, "length: should be greater than 0\n");
				continue;
			} else if ((slave_addr + len) < 0 || (slave_addr + len) >= 0x7F) {
				fprintf(stderr, "length (%i) caused out of range for 7bit address (0x%x)\n",
					len, slave_addr);
				continue;
			}

			count += _command_scan(mod, slave_addr, slave_addr + len, 1);
		}
	}

	return count;
}

/**
 * Scan the bus for slave
 * @param cmd space seperated slave address
 * @note if cmd is NULL, scan for all possible slaves
 */
static bool command_scan(b0_i2c *mod, char *cmd)
{
	unsigned count = (cmd == NULL) ?
		_command_scan(mod, 0b0001000, 0b1111000, 1) :
		_command_scan_addr(mod, cmd);

	if (!count) {
		printf("Nothing found, go check the connections.\n");
	}

	return true;
}

/**
 * Print hardware information
 */
static bool command_hw(b0_i2c *mod, char *cmd)
{
	printf("Module: \"%s\"\n", mod->header.name);
	printf("SCK: \"%s\"\n", mod->label.sck);
	printf("SDA: \"%s\"\n", mod->label.sda);
	printf("Voltage: %.7g to %.7g volts\n", mod->ref.low, mod->ref.high);

	if (mod->version.count) {
		const char *special[8] = {
			/* see: https://en.wikipedia.org/wiki/I%C2%B2C */
			"Standard-mode Sm (100Khz)",
			"Fast-mode Fm v1 (400Khz)",
			"High-speed mode Hs v2 (3.4 MHz)",
			"High-speed mode v2.1 (3.4 MHz)",
			"Fast-mode plus Fm+ V3 (1 MHz)",
			"Ultra Fast-mode UFM v4 (5 MHz)",
			"Ultra Fast-mode UFM v5 (5 MHz)"
			"Ultra Fast-mode UFM v6 (5 MHz)"
		};

		printf("Version:\n");
		for (size_t i = 0; i < mod->version.count; i++) {
			uint8_t value = mod->version.values[i];
			if (value < 8) {
				printf(" %s\n", special[value]);
			} else {
				printf(" %"PRIX8"\n", value);
			}
		}
	} else {
		printf("[list of Version not provided]");
	}

	return true;
}

static bool command_id(b0_i2c *mod, char *cmd)
{
	char *token;
	while ((token = strsep(&cmd, " ")) != NULL) {
		char *endptr;

		errno = 0;
		int slave_addr = strtol(token, &endptr, 0);
		/* TODO: 10bit address support */

		if (errno) {
			fprintf(stderr, "strtol failed\n");
			return false;
		} else if (endptr == cmd) {
			fprintf(stderr, "no digits found\n");
			return false;
		} else if (slave_addr < 0 ||slave_addr >= 0b1111000) {
			fprintf(stderr, "not a valid 7bit address: \"%s\"\n", token);
			return false;
		}

		uint16_t manuf, part;
		uint8_t rev;

		const b0_i2c_sugar_arg arg = {
			.addr = slave_addr,
			.version = B0_I2C_VERSION_SM /* FIXME: configurable */
		};

		if (B0_ERR_RC(b0_i2c_master_slave_id(mod, &arg, &manuf, &part, &rev))) {
			fprintf(stderr, "unable to read slave id (0x%"PRIX8")\n", slave_addr);
			return false;
		}

		printf("Slave 0x%"PRIx8"\n", slave_addr);
		printf("   Manufacturer: 0x%"PRIx16"\n", manuf);
		printf("   Part: 0x%"PRIx16"\n", part);
		printf("   Revision: 0x%"PRIx8"\n", rev);
	}

	return true;
}

//~ static bool command_exec(b0_i2c *mod, char *cmd)
//~ {

//~ }

static bool command_read(b0_i2c *mod, char *cmd)
{
	char *endptr, *token = strsep(&cmd, " ");

	errno = 0;
	int slave_addr = strtol(token, &endptr, 0);
	/* TODO: 10bit address support */

	if (errno) {
		fprintf(stderr, "strtol failed\n");
		return false;
	} else if (endptr == token) {
		fprintf(stderr, "no digits found\n");
		return false;
	} else if (slave_addr < 0 || slave_addr >= 0b1111000) {
		fprintf(stderr, "not a valid 7bit address: \"%s\"\n", token);
		return false;
	}

	errno = 0;
	int len = strtol(cmd, &endptr, 0);
	if (errno) {
		fprintf(stderr, "strtol failed\n");
		return false;
	} else if (endptr == cmd) {
		fprintf(stderr, "no digits found\n");
		return false;
	}

	if (len <= 0) {
		fprintf(stderr, "length should be greather than 0\n");
		return false;
	} else if (len > 255) {
		fprintf(stderr, "length cannot be greather than 255\n");
		return false;
	}

	uint8_t data[255];

	const b0_i2c_sugar_arg arg = {
		.addr = slave_addr,
		.version = B0_I2C_VERSION_SM /* FIXME: configurable */
	};

	if (B0_ERR_RC(b0_i2c_master_read(mod, &arg, data, len))) {
		fprintf(stderr, "failed to read %u bytes\n", len);
		return false;
	}

	printf("data: ");
	for (size_t i = 0; i < len; i++) {
		printf("%s0x%"PRIx8, i ? ", " : "", data[i]);
	}
	putchar('\n');

	return true;
}

static bool command_write(b0_i2c *mod, char *cmd)
{
	char *endptr, *token = strsep(&cmd, " ");

	if (token == NULL) {
		fprintf(stderr, "address missing\n");
		return false;
	}

	errno = 0;
	int slave_addr = strtol(token, &endptr, 0);
	/* TODO: 10bit address support */

	if (errno) {
		fprintf(stderr, "strtol failed\n");
		return false;
	} else if (endptr == token) {
		fprintf(stderr, "no digits found\n");
		return false;
	} else if (slave_addr < 0 || slave_addr >= 0b1111000) {
		fprintf(stderr, "not a valid 7bit address: \"%s\"\n", token);
		return false;
	}

	int len = 0;
	uint8_t data[255];
	while ((token = strsep(&cmd, " ")) != NULL) {
		if (len >= 255) {
			fprintf(stderr, "length cannot be greater than 255\n");
			return false;
		}

		errno = 0;
		int d = strtol(token, &endptr, 0);

		if (errno) {
			fprintf(stderr, "strtol failed\n");
			return false;
		} else if (endptr == cmd) {
			fprintf(stderr, "no digits found\n");
			return false;
		} if (d <= 0 || d > 255) {
			fprintf(stderr, "not a 8bit value: \"%s\"\n", token);
			return false;
		}

		data[len++] = d;
	}

	if (!len) {
		fprintf(stderr, "provide some data to send!\n");
		return false;
	}

	const b0_i2c_sugar_arg arg = {
		.addr = slave_addr,
		.version = B0_I2C_VERSION_SM /* FIXME: configurable */
	};

	if (B0_ERR_RC(b0_i2c_master_write(mod, &arg, data, len))) {
		fprintf(stderr, "failed to write %u bytes\n", len);
		return false;
	}

	printf("successfully written %u bytes\n", len);
	return true;
}

static bool command_write8read(b0_i2c *mod, char *cmd)
{
	char *endptr, *token = strsep(&cmd, " ");

	errno = 0;
	int slave_addr = strtol(token, &endptr, 0);
	/* TODO: 10bit address support */

	if (errno) {
		fprintf(stderr, "strtol failed\n");
		return false;
	} else if (endptr == token) {
		fprintf(stderr, "no digits found\n");
		return false;
	} else if (slave_addr < 0 || slave_addr >= 0b1111000) {
		fprintf(stderr, "not a valid 7bit address: \"%s\"\n", token);
		return false;
	}

	errno = 0;
	token = strsep(&cmd, " ");
	int write = strtol(token, &endptr, 0);
	if (errno) {
		fprintf(stderr, "strtol failed\n");
		return false;
	} else if (endptr == cmd) {
		fprintf(stderr, "no digits found\n");
		return false;
	} else if (write < 0 || write > 255) {
		fprintf(stderr, "not an 8bit value: \"%s\"\n", token);
		return false;
	}

	errno = 0;
	int len = strtol(cmd, &endptr, 0);
	if (errno) {
		fprintf(stderr, "strtol failed\n");
		return false;
	} else if (endptr == cmd) {
		fprintf(stderr, "no digits found\n");
		return false;
	}

	if (len <= 0) {
		fprintf(stderr, "length should be greather than 0\n");
		return false;
	} else if (len > 255) {
		fprintf(stderr, "length greather than 254\n");
		return false;
	}

	uint8_t data[255];

	const b0_i2c_sugar_arg arg = {
		.addr = slave_addr,
		.version = B0_I2C_VERSION_SM /* FIXME: configurable */
	};

	if (B0_ERR_RC(b0_i2c_master_write8_read(mod, &arg, write, data, len))) {
		fprintf(stderr, "failed to read %u bytes\n", len);
		return false;
	}

	printf("data: ");
	for (size_t i = 0; i < len; i++) {
		printf("%s0x%"PRIx8, i ? ", " : "", data[i]);
	}
	putchar('\n');

	return true;
}

struct cmd_list_value {
	const char *name;
	bool (*cb)(b0_i2c *mod, char *cmd);
};

/* list of command supported */
static const struct cmd_list_value cmd_list[] = {
	{"copyright", command_copyright},
	{"warranty", command_warranty},
	{"?", command_help},
	{"help", command_help},
	{"scan", command_scan},
	{"hw", command_hw},
	{"id", command_id},
	{"read", command_read},
	{"r", command_read},
	{"write", command_write},
	{"w", command_write},
	{"write8read", command_write8read},
	{"w8r", command_write8read},
	{NULL}
};

/**
 * Execute the @a cmd command and return the status
 * @param mod Module
 * @param cmd Command to execute
 * @return true on success, false on failure
 */
static bool execute_command_and_ret_status(b0_i2c *mod, char *cmd)
{
	const char *name = strsep(&cmd, " ");
	if (name == NULL) {
		fprintf(stderr, "Unknown input: %p\n", cmd);
		return false;
	}

	const struct cmd_list_value *i = cmd_list;
	while (i->name != NULL) {
		if (!strcmp(name, i->name)) {
			return i->cb(mod, cmd);
		}

		i++;
	}

	fprintf(stderr, "Unknown command: %s\n", name);
	return false;
}

static void show_repl(b0_i2c *mod)
{
	rl_attempted_completion_function = the_completion;

	const char *notice =
		"b0i2c Copyright (C) 2016  Kuldeep Singh Dhaka\n"
		"This program comes with ABSOLUTELY NO WARRANTY; for details type `warranty'.\n"
		"This is free software, and you are welcome to redistribute it\n"
		"under certain conditions; type `copyright' for details.\n"
		"\n"
		"type `help' for Help\n";

	puts(notice);

	for (;;) {
		/* Display prompt and read input (n.b. input must be freed after use)... */
		char *input = readline("Box0 I2C) ");

		/* Configure readline to auto-complete paths when the tab key is hit. */
		rl_bind_key('\t', rl_complete);

		/* Check for EOF. */
		if (!input) {
			break;
		}

		/* empty command? */
		if (!strlen(input)) {
			free(input);
			continue;
		}

		/* hardwired in genes to exit */
		if (!strcmp(input, "exit")) {
			free(input);
			break;
		}

		/* Add input to history. */
		add_history(input);

		/* Do stuff... */
		if (!execute_command_and_ret_status(mod, input)) {
			fprintf(stderr, "Command failed: %s\n", input);
		}

		/* Free input. */
		free(input);
	}
}

/* autocomplete */

static char **the_completion(const char * text , int start,  int end)
{
	if (start == 0) {
		return rl_completion_matches(text, &the_generator);
	}

	rl_bind_key('\t',rl_abort);
	return NULL;
}

static char *the_generator(const char *text, int state)
{
	static const struct cmd_list_value *index;
	static size_t len;
	const char *name;

	/* initalize (only once per text) */
	if (!state) {
		index = cmd_list;
		len = strlen(text);
	}

	while ((name = index->name) != NULL) {
		index++;
		if (!strncmp(name, text, len)) {
			return strdup(name);
		}
	}

	/* no match found marker */
	return NULL;
}
