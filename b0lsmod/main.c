/*
 * This file is part of box0-utils.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <argp.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>

#include "../common-code/sigkill.c"

/*
 * compile: gcc main.c -o b0lsmod -lbox0
 */

const char *argp_program_version = "b0lsmod 0.1.0";
const char *argp_program_bug_address = "<kuldeep@madresistor.com>";

/* Program documentation. */
static char doc[] =
	"Box0 list modules -- a program to list modules of a device\v"
	"MODULE_INDEX default = 0\n"
	"MODULE_TYPE should be {AIN, AOUT, DIO, I2C, SPI, UNIO, PWM}\n"
	"MODULE_TYPE is case insensitive\n"
	"All debug information is printed on stderr";

/* A description of the arg we accept. */
static char args_doc[] = "MODULE_TYPE...";

/* The options we understand. */
static struct argp_option options[] = {
	{"index",	'i', "MODULE_INDEX",      0,  "Module index" },
	{"verbose",	'v', 0,      0,  "Verbose output" },
	/* TODO: when verbose is true, print modules properties */
	{ 0 }
};

/* Used by main to communicate with parse_opt. */
struct argument {
	size_t types_len;
	b0_module_type *types;
	int index;
	int verbose;
};

struct module_type_map {
	const char *name;
	b0_module_type type;
} MODULE_TYPE_MAP[] = {
	{"ain", B0_AIN},
	{"aout", B0_AOUT},
	{"spi", B0_SPI},
	{"i2c", B0_I2C},
	{"pwm", B0_PWM},
	{"dio", B0_DIO},
	{NULL, 0},
};

/* Parse a single option. */
static error_t parse_opt (int key, char *str_arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	 know is a pointer to our arg structure. */
	struct argument *arg = state->input;

	switch (key) {
	case 'v':
		arg->verbose = 1;
	break;

	case 'i': {
		long int index = atol(str_arg);
		if (!(index >= 0 && index < 256)) {
			argp_usage (state);
		} else {
			arg->index = index;
		}
	} break;

	case ARGP_KEY_ARG: {
		struct module_type_map *i;
		for (i = MODULE_TYPE_MAP; i->name != NULL; i++) {
			if (!strcasecmp(str_arg, i->name)) {
				size_t bytes = (arg->types_len + 1) * sizeof(arg->types);
				arg->types = realloc(arg->types, bytes);
				arg->types[arg->types_len++] = i->type;
				break;
			}
		}

		/* no match found! */
		if (i->name == NULL) {
			argp_usage (state);
		}
	} break;

	case ARGP_KEY_END:
	break;

	default:
	return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc};

int main (int argc, char **argv)
{
	struct argument arg;
	b0_device *dev;
	size_t i, j;

	/* disable logging using env log.  (if user has not supplied any) */
	setenv("LIBBOX0_LOG", "none", 0);

	/* Default values. */
	arg.types_len = 0;
	arg.types = NULL;
	arg.verbose = 0;
	arg.index = -1;

	/* Parse our arg; every option seen by parse_opt will
	 be reflected in arg. */
	argp_parse (&argp, argc, argv, 0, 0, &arg);

	/* open device */
	if (B0_ERROR_RESULT_CODE(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		goto done;
	}

	for (i = 0; i < dev->modules_len; i++) {
		b0_module *mod = dev->modules[i];

		/* index match */
		if (arg.index >= 0) {
			if (arg.index != mod->index) {
				continue;
			}
		}

		/* filter based on type */
		if (arg.types_len > 0) {
			for (j = 0; j < arg.types_len; j++) {
				if (arg.types[j] == mod->type) {
					break;
				}
			}

			/* user not interested */
			if (j == arg.types_len) {
				continue;
			}
		}

		/* get a stringified type */
		const char *type = "???";
		struct module_type_map *k;
		for (k = MODULE_TYPE_MAP; k->name != NULL; k++) {
			if (k->type == mod->type) {
				type = k->name;
				break;
			}
		}

		/* printing information */
		printf("%s:%u %s\n", type, mod->index, mod->name);
	}

	close_device:
	if (B0_ERROR_RESULT_CODE(b0_device_close(dev))) {
		fprintf(stderr, "unable to close device\n");
	}

	done:
	exit (0);
}
