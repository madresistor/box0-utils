/*
 * This file is part of box0-utils.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <argp.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>

#include "../common-code/sigkill.c"

/*
 * compile: gcc maout.c -o b0aout -lbox0
 *
 * ./b0aout 5000
 *
 * TODO: support M(mega), K(kilo), G(giga) for FREQ
 * TODO: support s(second) m(minute) h(hours) prefix for TIME
 */

static double square(double angle);
static double triangular(double angle);
static double saw_tooth(double angle);
static double noise(double arg);
static double constant(double arg);

const char *argp_program_version = "b0aout 0.1.0";
const char *argp_program_bug_address = "<kuldeep@madresistor.com>";

struct available_waveform {
	double (*func)(double);
	const char *name;
} waveforms[] = {
	{sin, "sine"},
	{square, "square"},
	{triangular, "triangular"},
	{noise, "noise"},
	{constant, "constant"},
	{saw_tooth, "saw-tooth"},
	{NULL}
};

/* Program documentation. */
static char doc[] =
	"Box0 AOUT -- a program to output waveform to Box0 AOUT\v"
	"UINT         Unsigned integer value (Mathematics: Natural Number)\n"
	"MODULE_INDEX UINT < 256\n"
	"REPEAT       UINT < 2^32 (0 = till signal not raised)\n"
	"CHANNEL      UINT < 256\n"
	"SPEED        0 < UINT < 2^32\n"
	"BITSIZE      0 < UINT < 256\n"
	"WAVEFORM     {sine, square, triangular, noise, constant, saw-tooth} (default: sine)\n"
	"FLOAT        Floating point value  (Mathematics: Real Number)\n"
	"FREQ         FLOAT > 0 Frequency            (Unit: Hz)\n"
	"FREQ2        FLOAT > 0 Sweep end frequency  (Unit: Hz)\n"
	"DURATION     FLOAT > 0 Sweep duration (Unit: Seconds)\n"
	"SWEEP_TYPE   {linear, log}  (default: linear)\n"
	"All debug information is printed on stderr";

/* A description of the arg we accept. */
static char args_doc[] = "[WAVEFORM] FREQ [FREQ2 DURATION [SWEEP_TYPE]]";

/* The options we understand. */
static struct argp_option options[] = {
	{0,0,0,0, "Signal Generation:"},
	{"offset",    'o', "FLOAT",        0, "Offset for waveform (default: 0)"},
	{"amplitude", 'a', "FLOAT",        0, "Amplitude for waveform (default: 1)"},
	{"phase",     'p', "FLOAT",        0, "Phase shift (degree) (default: 0)"},

	{0,0,0,0, "Output:"},
	{"repeat",    'r', "REPEAT",       0, "Waveform repetition count (default: 0)"},
	{"channel",   'c', "CHANNEL",      0, "Output on which channel (default: 0)"},
	{"bitsize",   'b', "BITSIZE",      0, "Bitsize to use (default: auto-detect)"},
	{"speed",     's', "SPEED",        0, "Sampling Frequency (stream only)"},

	{0,0,0,0, "Other:"},
	{"module",    'm', "MODULE_INDEX", 0, "Module index (default: 0)"},
	{"mode",      't', "{static, stream}",0, "AOUT mode to use (default: auto-detect)"},

	{NULL}
};

enum mode {
	MODE_UNDEF = 0,
	MODE_STATIC = 1,
	MODE_STREAM = 2
};

/* Used by maout to communicate with parse_opt. */
struct argument {
	int module_index;
	double offset, amplitude, phase;
	enum mode mode;
	unsigned int bitsize;
	unsigned long speed;
	unsigned long duration;
	unsigned long repeat;
	unsigned int channel;
	double (*waveform)(double);
	double freq_start, freq_end;
	bool sweep_log;
};

/* Parse a single option. */
static error_t parse_opt (int key, char *str_arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	 know is a pointer to our arg structure. */
	struct argument *arg = state->input;

	switch (key) {
	case 'm': {
		arg->module_index = atoi(str_arg);
	} break;

	case 'o':
		arg->offset = atof(str_arg);
	break;

	case 'a':
		arg->amplitude = atof(str_arg);
	break;

	case 'p':
		arg->phase = atof(str_arg);
	break;

	case 't':
		if (!strcmp("static", str_arg)) {
			arg->mode = MODE_STATIC;
		} else if (!strcmp("stream", str_arg)) {
			arg->mode = MODE_STREAM;
		} else {
			argp_usage(state);
		}
	break;

	case 'i':
		if (str_arg == NULL) {
			printf("linear\n" "log\n");
		}

	break;

	case 's': {
		long int val = atol(str_arg);
		if (val >= 0) {
			arg->speed = val;
		} else {
			argp_usage(state);
		}
	} break;

	case 'b': {
		unsigned int value = atoi(str_arg);
		if (value >= 0) {
			arg->bitsize = value;
		} else {
			argp_usage(state);
		}
	} break;

	case 'r': {
		long int value = atol(str_arg);
		if (value >= 0) {
			arg->repeat = value;
		} else {
			argp_usage(state);
		}
	} break;

	case ARGP_KEY_ARG: {
		int go_for_arg;
		if (state->arg_num == 0) {
			go_for_arg = isalpha(str_arg[0]) ? 0 : 1;
		} else {
			go_for_arg = state->arg_num;
			if (arg->waveform == NULL) {
				go_for_arg++;
			}
		}

		if (go_for_arg == 0) {
			/* waveform */
			struct available_waveform *i;
			for (i = waveforms; i->func != NULL; i++) {
				if (!strcmp(i->name, str_arg)) {
					arg->waveform = i->func;
					break;
				}
			}
			if (i->func == NULL) {
				argp_usage(state);
			}
		} else if (go_for_arg == 1) {
			arg->freq_start = atof(str_arg);
			if (!(arg->freq_start > 0)) {
				argp_usage(state);
			}
		} else if (go_for_arg == 2) {
			arg->freq_end = atof(str_arg);
			if (!(arg->freq_end > 0)) {
				argp_usage(state);
			}
		} else if (go_for_arg == 3) {
			long int value = atoi(str_arg);
			if (value >= 0) {
				arg->duration = value;
			} else {
				argp_usage(state);
			}
		} else if (go_for_arg == 4) {
			if (!strcmp("log", str_arg)) {
				arg->sweep_log = true;
			} else if (!strcmp("linear", str_arg)) {
				arg->sweep_log = false;
			} else {
				argp_usage(state);
			}
		} else {
			argp_usage(state);
		}
	} break;

	case ARGP_KEY_END:
		if (arg->freq_start == 0 || (arg->freq_end != 0 && arg->duration == 0)) {
			/* Not enough arg. */
			argp_usage(state);
		}
	break;

	default:
	return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

void data_sweep_none(struct argument *arg, uint32_t sample_rate, double *data, size_t count)
{
	size_t i;
	double two_pi = 2.0 * M_PI;

	for (i = 0; i < count; i++) {
		double x = two_pi * arg->freq_start * i;
		x = (x + arg->phase) / sample_rate;
		data[i] = (arg->waveform(x) * arg->amplitude) + arg->offset;
	}
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc};

int main (int argc, char **argv)
{
	struct argument arg;
	b0_device *device;
	b0_aout *module;
	b0_result_code rc;
	size_t i;

	/* Default values. */
	arg.module_index = 0;
	arg.offset = 0;
	arg.amplitude = 1;
	arg.phase = 0;
	arg.mode = MODE_UNDEF;
	arg.bitsize = 0;
	arg.duration = 0;
	arg.repeat = 0;
	arg.channel = 0;
	arg.waveform = NULL;
	arg.freq_start = arg.freq_end = 0;
	arg.sweep_log = false;

	/* Parse our arg; every option seen by parse_opt will
	 be reflected in arg. */
	argp_parse (&argp, argc, argv, 0, 0, &arg);

	if (arg.waveform == NULL) {
		/* default */
		arg.waveform = sin;
	}

	printf("module_index: %"PRIu8"\n", arg.module_index);
	printf("offset: %f\n", arg.offset);
	printf("amplitude: %f\n", arg.amplitude);
	printf("phase: %f\n", arg.phase);
	printf("bitsize: %"PRIu8"\n", arg.bitsize);
	printf("mode: %i\n", arg.mode);
	printf("duration: %lu\n", arg.duration);
	printf("repeat: %u\n", arg.repeat);
	printf("channel: %"PRIu8"\n", arg.channel);
	printf("waveform: %p\n", arg.waveform);
	printf("freq_start: %f\n", arg.freq_start);
	printf("freq_end: %f\n", arg.freq_end);
	printf("sweep: %s\n", arg.sweep_log ? "log" : "linear");


	/* open device */
	rc = b0_usb_open_supported(&device);
	if (B0_ERROR_RESULT_CODE(rc)) {
		fprintf(stderr, "unable to open device\n");
		goto done;
	}

	/* open module */
	rc = b0_aout_open(device, &module, arg.module_index);
	if (B0_ERROR_RESULT_CODE(rc)) {
		fprintf(stderr, "unable to open module\n");
		goto close_device;
	}

	if (module->snapshot.count) {
		arg.bitsize = module->snapshot.values[0].bitsize;
	} else {
		fprintf(stderr, "Snapshot mode not supported\n");
		goto close_module;
	}

	unsigned long speed;
	size_t count;
	double *data;

	rc = b0_aout_snapshot_calc(module, arg.freq_start, arg.bitsize, &count, &speed);
	if (B0_ERROR_RESULT_CODE(rc)) {
		fprintf(stderr, "Snapshot mode calc failed\n");
		goto close_module;
	}

	data = malloc(sizeof(double) * count);
	if (data == NULL) {
		fprintf(stderr, "malloc returned NULL\n");
		goto close_module;
	}

	data_sweep_none(&arg, speed, data, count);

	/* prepare for static */
	if (B0_ERR_RC(b0_aout_snapshot_prepare(module))) {
		fprintf(stderr, "unable to prepare module for static \n");
		goto close_module;
	}

	if (B0_ERR_RC(b0_aout_repeat_set(module, arg.repeat))) {
		fprintf(stderr, "unable to set repeat\n");
		goto close_module;
	}

	if (B0_ERR_RC(b0_aout_bitsize_speed_set(module, arg.bitsize, speed))) {
		fprintf(stderr, "unable to set bitsize\n");
		goto close_module;
	}

	if (B0_ERR_RC(b0_aout_snapshot_start_double(module, data, count))) {
		fprintf(stderr, "unable to start module arbitary \n");
		goto close_module;
	}

	sigkill_block_till_not_served();

	if (B0_ERR_RC(b0_aout_snapshot_stop(module))) {
		fprintf(stderr, "unable to stop module\n");
	}

	close_module:
	if (B0_ERROR_RESULT_CODE(b0_aout_close(module))) {
		fprintf(stderr, "unable to close module\n");
	}

	close_device:
	if (B0_ERROR_RESULT_CODE(b0_device_close(device))) {
		fprintf(stderr, "unable to close device\n");
	}

	done:
	exit (0);
}

static double square(double angle)
{
	angle = fmodf(angle, (2 * M_PI));

	return (angle >= M_PI) ? -1 : 1;
}

static double triangular(double angle)
{
	angle = fmodf(angle, (2 * M_PI));

	if (angle < (M_PI_2)) {
		return angle / M_PI_2;
	} else if (angle < (3 * M_PI_2)) {
		return 2 - (angle / M_PI_2);
	} else {
		return (angle / M_PI_2) - 4;
	}
}

static double saw_tooth(double angle)
{
	angle = fmodf(angle, (2 * M_PI));

	if (angle < M_PI) {
		return angle / M_PI;
	} else {
		return (angle / M_PI) - 2;
	}
}

static double noise(double arg)
{
	(void)arg;

	/* Ref: http://stackoverflow.com/questions/2704521 */
	double f = (double)rand() / RAND_MAX;
	return -1.0 + f * 2.0;
}

static double constant(double arg)
{
	(void)arg;
	return 0;
}
