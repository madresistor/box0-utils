#
# This file is part of box0-utils.
# Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# box0-utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# box0-utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
#

FIND_PACKAGE(libbox0 REQUIRED)
INCLUDE_DIRECTORIES(${LIBBOX0_INCLUDE_DIRS})
ADD_DEFINITIONS(${LIBBOX0_DEFINITIONS})

ADD_EXECUTABLE(b0pwm "${CMAKE_CURRENT_SOURCE_DIR}/main.c")

TARGET_LINK_LIBRARIES(b0pwm ${LIBBOX0_LIBRARIES})

INSTALL(TARGETS b0pwm RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}")
