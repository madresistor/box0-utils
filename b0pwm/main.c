/*
 * This file is part of box0-utils.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <argp.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>

#include "../common-code/sigkill.c"

/*
 * compile: gcc main.c -o b0pwm -lbox0
 *
 * ./b0pwm 5000 50
 *
 * TODO: support M(mega), K(kilo), G(giga) for FREQ
 */

const char *argp_program_version = "b0pwm 0.1.0";
const char *argp_program_bug_address = "<kuldeep@madresistor.com>";

/* Program documentation. */
static char doc[] =
	"Box0 PWM -- a program to control box0 pwm\v"
	"MODULE_INDEX default = 0\n"
	"CHANNEL_INDEX >= 0, default = 0\n"
	"FREQ > 0\n"
	"DUTY_CYCLE > 0 and < 100, default = 50\n"
	"MAX_ERROR >= 0 and < 100, default = 5";

/* A description of the arg we accept. */
static char args_doc[] = "FREQ [DUTY_CYCLE [MAX_ERROR]]";

/* The options we understand. */
static struct argp_option options[] = {
	{"module",	'm', "MODULE_INDEX",0,  "Module index"},
	{"channel",	'c', "CHANNEL_INDEX",      0,  "Output channel"},
	{"verbose",  'v', 0,       0, "Produce verbose output" },
	{ 0 }
};

/* Used by main to communicate with parse_opt. */
struct argument {
	uint8_t module_index;
	uint8_t channel_index;
	double freq;
	double duty_cycle;
	double max_error;
	int verbose;
};

/* Parse a single option. */
static error_t parse_opt (int key, char *str_arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	 know is a pointer to our arg structure. */
	struct argument *arg = state->input;

	switch (key) {

	case 'c': {
		long int index = atoi(str_arg);
		if (index >= 0) {
			arg->channel_index = index;
		} else {
			argp_usage (state);
		}
	} break;

	case 'm': {
		arg->module_index = atoi(str_arg);
	} break;

	case 'v':
		arg->verbose = 1;
	break;

	case ARGP_KEY_ARG: {
		double val = atof(str_arg);
		switch(state->arg_num) {
		case 0: /* FREQ */
			if (val > 0) {
				arg->freq = val;
			} else {
				argp_usage (state);
			}
		break;
		case 1: /* DUTY_CYCLE */
			if (val > 0 && val < 100) {
				arg->duty_cycle = val;
			} else {
				argp_usage (state);
			}
		break;
		case 2: /* MAX_ERROR */
			if (val >= 0 && val < 100) {
				arg->max_error = val;
			} else {
				argp_usage (state);
			}
		}
	} break;

	case ARGP_KEY_END:
		if (state->arg_num < 1) {
			/* Not enough arg. */
			argp_usage (state);
		}
	break;

	default:
	return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc};

int main (int argc, char **argv)
{
	struct argument arg;
	b0_device *device;
	b0_pwm *module;
	size_t i;

	sigkill_init();

	/* Default values. */
	arg.verbose = 0;
	arg.freq = 0;
	arg.duty_cycle = 50;
	arg.max_error = 5;
	arg.module_index = 0;
	arg.channel_index = 0;

	/* Parse our arg; every option seen by parse_opt will
	 be reflected in arg. */
	argp_parse (&argp, argc, argv, 0, 0, &arg);

	/* open device */
	if (B0_ERROR_RESULT_CODE(b0_usb_open_supported(&device))) {
		fprintf(stderr, "unable to open device\n");
		goto done;
	}

	/* open module */
	if (B0_ERROR_RESULT_CODE(b0_pwm_open(device, &module, arg.module_index))) {
		fprintf(stderr, "unable to open module\n");
		goto close_device;
	}

	/* prepare module */
	if (B0_ERROR_RESULT_CODE(b0_pwm_output_prepare(module))) {
		fprintf(stderr, "unable to prepare module\n");
		goto close_module;
	}

	/* set value */
	double freq = arg.freq;
	double duty_cycle = arg.duty_cycle;
	double max_error = arg.max_error;

	unsigned int bitsize = module->bitsize.values[0];
	unsigned long speed;
	b0_pwm_reg period;
	if (B0_ERR_RC(b0_pwm_output_calc(module, bitsize, freq, &speed, &period,
							max_error, true))) {
		fprintf(stderr, "unable to perform calculation\n");
		goto close_module;
	}

	if (B0_ERR_RC(b0_pwm_speed_set(module, speed))) {
		fprintf(stderr, "unable to set speed\n");
		goto close_module;
	}

	if (B0_ERR_RC(b0_pwm_period_set(module, period))) {
		fprintf(stderr, "unable to set period\n");
		goto close_module;
	}

	b0_pwm_reg width = B0_PWM_OUTPUT_CALC_WIDTH(period, arg.duty_cycle);
	if (B0_ERR_RC(b0_pwm_width_set(module, 0, width))) {
		fprintf(stderr, "unable to set width\n");
		goto close_module;
	}

	if (arg.verbose) {
		freq = B0_PWM_OUTPUT_CALC_FREQ(speed, period);
		max_error = B0_PWM_OUTPUT_CALC_FREQ_ERR(arg.freq, freq);
		duty_cycle = B0_PWM_OUTPUT_DUTY_CYCLE(period, width);
		printf("Requested Frequency: %f\n", arg.freq);
		printf("Output Frequency: %f\n", freq);
		printf("Max Error: %f\n", arg.max_error);
		printf("Actual Duty Cycle: %f\n", duty_cycle);
		printf("Actual Error: %f\n", max_error);
	}

	/* start module */
	if (B0_ERROR_RESULT_CODE(b0_pwm_output_start(module))) {
		fprintf(stderr, "unable to start\n");
		goto close_module;
	}

	sigkill_block_till_not_served();

	/* stop module */
	if (B0_ERROR_RESULT_CODE(b0_pwm_output_stop(module))) {
		fprintf(stderr, "unable to stop\n");
	}

	close_module:
	if (B0_ERROR_RESULT_CODE(b0_pwm_close(module))) {
		fprintf(stderr, "unable to close module\n");
	}

	close_device:
	if (B0_ERROR_RESULT_CODE(b0_device_close(device))) {
		fprintf(stderr, "unable to close device\n");
	}

	done:
	exit (0);
}
