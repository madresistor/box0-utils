/*
 * This file is part of box0-utils.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <argp.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>

#include "../common-code/sigkill.c"

/*
 * compile: gcc main.c -o b0ain2csv -lbox0
 *
 * ./b0ain2csv 12 96000
 *
 * TODO: support M(mega), K(kilo), G(giga) for SAMPLE_RATE
 * TODO: support s(second) m(minute) h(hours) prefix for TIME
 */

const char *argp_program_version = "b0ain2csv 0.1.0";
const char *argp_program_bug_address = "<kuldeep@madresistor.com>";

/* Program documentation. */
static char doc[] =
	"Box0 AIN to CSV -- a program to convert log box0 ain stream data to csv\v"
	"MODULE_INDEX default = 0\n"
	"TIME >= 0, if 0 infinite till signal not raised (in Seconds)\n"
	"PRECISION default is 6\n"
	"CHANNELS not given, {0} is assumed\n"
	"FILE not provided or is \"-\", write to stdout\n"
	"All debug information is printed on stderr";

/* A description of the arg we accept. */
static char args_doc[] = "BITSIZE SAMPLE_RATE CHANNEL...";

/* The options we understand. */
static struct argp_option options[] = {
	{"module",	'm', "MODULE_INDEX",0,  "Module index" },
	{"header",	'h', 0,      0,  "Include header in output" },
	{"comment",	'c', 0,      0,  "Include comment in output" },
	{"time",	't', "TIME",   0,  "Time to log data" },
	{"precision",'p', "PRECISION",0,  "Precision of print floating value" },
	{"output",	'o', "FILE", 0,
	"Output to FILE instead of standard output" },
	{ 0 }
};

/* Used by main to communicate with parse_opt. */
struct argument {
	unsigned int bitsize;
	int module_index;
	unsigned long sample_rate;
	int header, comment;
	unsigned long time;
	char *output_file;
	int precision;

	size_t channels_len;
	unsigned int *channels;
};

/* Parse a single option. */
static error_t parse_opt (int key, char *str_arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	 know is a pointer to our arg structure. */
	struct argument *arg = state->input;

	switch (key) {
	case 'h':
		arg->header = 1;
	break;

	case 'c':
		arg->comment = 1;
	break;

	case 'o':
		arg->output_file = str_arg;
	break;

	case 'p':
		arg->precision = atoi(str_arg);
	break;

	case 'm': {
		arg->module_index = atoi(str_arg);
	} break;

	case 't': {
		long int time = atol(str_arg);
		if (time < 0) {
			argp_usage (state);
		} else {
			arg->time = time;
		}
	} break;

	case ARGP_KEY_ARG: {
		long int val = atol(str_arg);
		if (val < 0) {
			argp_usage (state);
		} else {
			if (state->arg_num == 0) {
				arg->bitsize = val;
			} else if (state->arg_num == 1) {
				arg->sample_rate = val;
			} else {
				arg->channels_len++;
				arg->channels = realloc(arg->channels,
							arg->channels_len * sizeof(*arg->channels));
				arg->channels[arg->channels_len - 1] = val;
			}
		}
	} break;

	case ARGP_KEY_END:
		if (state->arg_num < 2) {
			/* Not enough arg. */
			argp_usage (state);
		}

		if (arg->channels_len == 0) {
			arg->channels_len = 1;
			arg->channels = malloc(1);
			arg->channels[0] = 0;
		}
	break;

	default:
	return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc};

int main (int argc, char **argv)
{
	struct argument arg;
	b0_device *device;
	b0_ain *module;
	size_t i;

	/* Default values. */
	arg.comment = 0;
	arg.header = 0;
	arg.time = 0;
	arg.module_index = 0;
	arg.output_file = "-";
	arg.channels = NULL;
	arg.channels_len = 0;
	arg.precision = 6;

	/* Parse our arg; every option seen by parse_opt will
	 be reflected in arg. */
	argp_parse (&argp, argc, argv, 0, 0, &arg);

	FILE *file;
	if (!strcmp("-", arg.output_file)) {
		file = stdout;
	} else {
		file = fopen(arg.output_file, "w");
		if (file == NULL) {
			fprintf(stderr, "unable to open file \"%s\"\n", arg.output_file);
			goto done;
		}
	}

	/* open device */
	if (B0_ERROR_RESULT_CODE(b0_usb_open_supported(&device))) {
		fprintf(stderr, "unable to open device\n");
		goto done;
	}

	/* open module */
	if (B0_ERROR_RESULT_CODE(b0_ain_open(device, &module, arg.module_index))) {
		fprintf(stderr, "unable to open module\n");
		goto close_device;
	}

	/* prepare module */
	if (B0_ERROR_RESULT_CODE(b0_ain_stream_prepare(module))) {
		fprintf(stderr, "unable to prepare for stream\n");
		goto close_module;
	}

	if (B0_ERROR_RESULT_CODE(b0_ain_chan_seq_set(module,
				arg.channels, arg.channels_len))) {
		fprintf(stderr, "unable to set channel sequence\n");
		goto close_module;
	}

	if (B0_ERROR_RESULT_CODE(b0_ain_bitsize_speed_set(module,
						arg.bitsize, arg.sample_rate))) {
		fprintf(stderr, "unable to set bitsize,speed\n");
		goto close_module;
	}

	if (arg.comment) {
		fprintf(file, "# Sample Rate %"PRIu32"\n", arg.sample_rate);
		fprintf(file, "# Bitsize %"PRIu8"\n", arg.bitsize);
		fprintf(file, "# Time %"PRIu32" Sec\n", arg.time);
		fprintf(file, "# Precision %"PRIu8"\n", arg.precision);
		fprintf(file, "# Channel");
		for (i = 0; i < arg.channels_len; i++) {
			fprintf(file, (i > 0) ? ", %"PRIu8 : " %"PRIu8,
				arg.channels[i]);
		}
		fputc('\n', file);
	}

	if (arg.header) {
		for (i = 0; i < arg.channels_len; i++) {
			unsigned int num = arg.channels[i];
			if (i > 0) {
				fputc(',', file);
			}

			if (module->label.chan[num] != NULL) {
				fprintf(file, "%s", module->label.chan[num]);
			} else {
				fprintf(file, "CH%u", i);
			}
		}

		fputc('\n', file);
	}

	/* prepare module */
	if (B0_ERROR_RESULT_CODE(b0_ain_stream_start(module))) {
		fprintf(stderr, "unable to prepare for stream\n");
		goto close_module;
	}

	size_t count = arg.sample_rate;
	double *arr = (double *) malloc(sizeof(double) * count);
	size_t got = 0;
	sigkill_init();
	while (!sigkill_served()) {
		/* read from module */
		if (B0_ERROR_RESULT_CODE(b0_ain_stream_read_double(module,
									arr, count, NULL))) {
			fprintf(stderr, "unable to read from stream\n");
			break;
		}

		/* print data */
		for (i = 0; i < count; i++) {
			got++;
			fprintf(file, "%.*f", arg.precision, arr[i]);
			size_t chan_num = got % arg.channels_len;
			char sep = (chan_num > 0) ? ',' : '\n';
			fputc(sep, file);
		}

		/* number of second to acquire */
		if (arg.time == 1) {
			break;
		} else if (arg.time > 1) {
			arg.time--;
		}
	}

	if (B0_ERROR_RESULT_CODE(b0_ain_stream_stop(module))) {
		fprintf(stderr, "unable to stop module\n");
	}

	close_module:
	if (B0_ERROR_RESULT_CODE(b0_ain_close(module))) {
		fprintf(stderr, "unable to close module\n");
	}

	close_device:
	if (B0_ERROR_RESULT_CODE(b0_device_close(device))) {
		fprintf(stderr, "unable to close device\n");
	}

	done:
	exit (0);
}
