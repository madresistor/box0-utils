/*
 * This file is part of box0-utils.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <argp.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/misc/box0-v5/box0-v5.h>
#include <libbox0/misc/box0-v5/power-supply.h>

const char *argp_program_version = "b0v5power 0.1.0";
const char *argp_program_bug_address = "<kuldeep@madresistor.com>";

/* Program documentation. */
static char doc[] =
	"box0-v5 -- a program to control box0-v5 power supply\v"
	"BOOLEAN can be {on, off, true, false, yes, no, enable, disable}\n"
	"All debug information is printed on stderr\n"
	"On/Off to hardware is send first and then status is printed [if requested]";

/* A description of the arg we accept. */
static char args_doc[] = "";

#define ASCII_PLUS_MINUS "+/-"
#define UTF8_PLUS_MINUS "±"

#define ARGP_PM5_MESSAGE(x) "Turn On/Off " x "5V [default: on]"
#define ASCII_ARGP_PM5_MESSAGE ARGP_PM5_MESSAGE(ASCII_PLUS_MINUS " ")
#define UTF8_PLUS_MINUS_MESSAGE ARGP_PM5_MESSAGE(UTF8_PLUS_MINUS)

/* The options we understand. */
static struct argp_option options[] = {
	{"pm5",	0, "BOOLEAN", OPTION_ARG_OPTIONAL, ASCII_ARGP_PM5_MESSAGE},
	{"analog",	'a', NULL, OPTION_ALIAS, NULL},
	{"p3v3",	0, "BOOLEAN", OPTION_ARG_OPTIONAL, "Turn On/Off +3.3V [default: on]"},
	{"digital",	'd', NULL, OPTION_ALIAS, NULL},
	{"status", 's', "BOOLEAN", OPTION_ARG_OPTIONAL, "Print status of supply [default: on]"},
	{ 0 }
};

enum bool_value {
	BOOL_UNDEF = -1, /* Undefined */
	BOOL_OFF = 0, /* Off */
	BOOL_ON = 1, /* On */
};

/* Used by main to communicate with parse_opt. */
struct argument {
	enum bool_value pm5;
	enum bool_value p3v3;
	enum bool_value status;
};

static enum bool_value get_bool_value(char *arg)
{
	size_t i;
	size_t len = 0;
	const char *match[] = {
		"off", "no", "disable", "false",
		"on", "yes", "enable", "true"
	};

	/* calculate length */
	if (arg != NULL) {
		len = strlen(arg);
	}

	/* empty argument is assumed as on */
	if (!len) {
		return BOOL_ON;
	}

	/* try to match */
	for (i = 0; i < 8; i++) {
		if (!strncasecmp(arg, match[i], len)) {
			return (i > 3) ? BOOL_ON : BOOL_OFF;
		}
	}

	/* error, no match found */
	return BOOL_UNDEF;
}

static int parse_boolean_argument(enum bool_value *store, char *str_arg)
{
	/* argument is already provided */
	if (*store != BOOL_UNDEF) {
		return ARGP_ERR_UNKNOWN;
	}

	/* try to extract boolean information */
	enum bool_value v = get_bool_value(str_arg);
	if (v == BOOL_UNDEF) {
		return ARGP_ERR_UNKNOWN;
	}

	/* store the value */
	*store = v;
	return 0;
}

/* Parse a single option. */
static error_t parse_opt (int key, char *str_arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	 know is a pointer to our arg structure. */
	struct argument *arg = state->input;

	switch (key) {
	case 'd':
	return parse_boolean_argument(&arg->p3v3, str_arg);
	case 'a':
	return parse_boolean_argument(&arg->pm5, str_arg);
	case 's':
	return parse_boolean_argument(&arg->status, str_arg);
	default:
	return ARGP_ERR_UNKNOWN;
	}
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc};

static bool utf8_supported()
{
	const char *LANG = getenv("LANG");
	return (LANG != NULL) && (strstr(LANG, ".UTF-8") != NULL);
}

int main (int argc, char **argv)
{
	struct argument arg;
	b0_device *dev;
	uint8_t mask;
	uint8_t value;
	bool utf8_support = utf8_supported();

	/* Default values. */
	arg.pm5 = BOOL_UNDEF;
	arg.p3v3 = BOOL_UNDEF;
	arg.status = BOOL_UNDEF;

	/* Parse our arg; every option seen by parse_opt will
	 be reflected in arg. */
	if (utf8_support) {
		options[0].doc = UTF8_PLUS_MINUS_MESSAGE;
	}
	argp_parse (&argp, argc, argv, 0, 0, &arg);

	if (arg.pm5 == BOOL_UNDEF &&
		arg.p3v3 == BOOL_UNDEF &&
		arg.status == BOOL_UNDEF) {
		goto done;
	}

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		goto done;
	}

	if (B0_ERR_RC(b0v5_valid_test(dev))) {
		fprintf(stderr, "opened device isnt box0-v5");
		/* TODO: open the next supported device */
		goto close;
	}

	mask |= (arg.pm5 != BOOL_UNDEF) ? B0V5_PS_PM5 : 0x00;
	mask |= (arg.p3v3 != BOOL_UNDEF) ? B0V5_PS_P3V3 : 0x00;

	value |= (arg.pm5 == BOOL_ON) ? B0V5_PS_PM5 : 0x00;
	value |= (arg.p3v3== BOOL_ON) ? B0V5_PS_P3V3 : 0x00;

	if (B0_ERR_RC(b0v5_ps_en_set(dev, mask, value))) {
		fprintf(stderr, "unable to enable supply\n");
		goto close;
	}

	print_status:
	/* skip if not requested */
	if (arg.status != BOOL_ON) {
		goto close;
	}

	if (B0_ERR_RC(b0v5_ps_en_get(dev, &mask))) {
		fprintf(stderr, "unable to read supply status\n");;
		goto close;
	}

	/* use LANG enviroment variable to detect if UTF-8 is supported */
	const char *pm = utf8_support ? UTF8_PLUS_MINUS : ASCII_PLUS_MINUS;
	printf("%s5V: %s\n", pm, (mask & B0V5_PS_PM5) ? "on" : "off");
	printf("+3.3V: %s\n", (mask & B0V5_PS_P3V3) ? "on" : "off");

	close:
	/* close device */
	b0_device_close(dev);

	done:
	exit (0);
}
