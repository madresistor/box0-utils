/*
 * This file is part of box0-utils.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_UTILS_COMMON_CODE_SIGKILL_H
#define BOX0_UTILS_COMMON_CODE_SIGKILL_H

#include <stdbool.h>
#include "sleep.c"

void sigkill_init(void);
bool sigkill_served(void);
void sigkill_block_till_not_served(void);

volatile static bool served = false;

#if defined(_WIN32) || defined(__MINGW32__)
/* http://stackoverflow.com/questions/16826097 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL WINAPI ConsoleHandler(DWORD dwType)
{
	if (dwType == CTRL_BREAK_EVENT || dwType == CTRL_C_EVENT) {
		served = true;
	}

	return TRUE;
}

void sigkill_init(void)
{
	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler, true)) {
		fprintf(stderr, "Unable to install handler!\n");
		exit(EXIT_FAILURE);
	}
}

#else
# include <signal.h>

void sighandler(int signum)
{
	served = true;
}

void sigkill_init(void)
{
	struct sigaction sigact;

	sigact.sa_handler = sighandler;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigaction(SIGINT, &sigact, NULL);
	sigaction(SIGTERM, &sigact, NULL);
	sigaction(SIGQUIT, &sigact, NULL);
}

#endif

bool sigkill_served(void)
{
	return served;
}

void sigkill_block_till_not_served(void)
{
	sigkill_init();
	while (!sigkill_served()) {
		sleep_milliseconds(500);
	}
}

#endif
