/*
 * This file is part of box0-utils.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-utils.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_UTILS_COMMON_CODE_SLEEP_H
#define BOX0_UTILS_COMMON_CODE_SLEEP_H

#if defined(_WIN32) || defined(__MINGW32__)
# include <windows.h>
# define sleep_seconds(x) Sleep(x * 1000)
# define sleep_milliseconds(x) Sleep(x)
#else /* defined(__MINGW32__) */
# include <unistd.h>
# define sleep_seconds(x) sleep(x)
# define sleep_milliseconds(x) usleep(x * 1000)
#endif

#endif
