# - Try to find readline
#
#  READLINE_FOUND - system has readline
#  READLINE_INCLUDE_DIRS - the readline include directory
#  READLINE_LIBRARIES - Link these to use readline
#  READLINE_DEFINITIONS - Compiler switches required for using readline
#
#  Adapted from cmake-modules Google Code project
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the New BSD license.
#
# CMake-Modules Project New BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(READLINE_INCLUDE_DIR
	NAMES readline/readline.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

# Note: Only searching for "readline.so"
FIND_LIBRARY(READLINE_LIBRARY
	NAMES readline
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (READLINE_INCLUDE_DIR AND READLINE_LIBRARY)
	SET(READLINE_FOUND TRUE)
	SET(READLINE_INCLUDE_DIRS ${READLINE_INCLUDE_DIR})
	SET(READLINE_LIBRARIES ${READLINE_LIBRARY})
ENDIF (READLINE_INCLUDE_DIR AND READLINE_LIBRARY)

IF (READLINE_FOUND)
	IF (NOT readline_FIND_QUIETLY)
		MESSAGE(STATUS "Found readline:")
		MESSAGE(STATUS " - Includes: ${READLINE_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${READLINE_LIBRARIES}")
	ENDIF (NOT readline_FIND_QUIETLY)
ELSE (READLINE_FOUND)
	IF (readline_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find readline")
	ENDIF (readline_FIND_REQUIRED)
ENDIF (READLINE_FOUND)

# show the READLINE_INCLUDE_DIRS and READLINE_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(READLINE_INCLUDE_DIRS READLINE_LIBRARIES)
